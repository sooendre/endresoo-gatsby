---
sideText: "Endre Soo"
poweredBy: "Professional full stack web development, maintenance, support and design.


Made in Gatsby"
links: [
	{
		title: "GitLab",
		svg: "gitlab",
		url: "https://gitlab.com/endre.soo"
	},
	{
		title: "CodePen",
		svg: "codepen",
		url: "https://codepen.io/endrukk_nudge"
	},
	{
		title: "LinkedIn",
		svg: "linkedin",
		url: "https://www.linkedin.com/in/endre-so%C3%B3-812869156/"
	},
	{
		title: "Mail me",
		svg: "mail",
		url: "mailto:contact@endresoo.com"
	},
	{
		title: "Twitter",
		svg: "twitter",
		url: "https://twitter.com/endrukk"
	},
]

---
