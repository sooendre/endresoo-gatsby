---
hero: {
	title: "I'm Endre Soo.",
	image: ../../assets/images/pexels-tim-gouw-52608.min.jpg,
}
intro: {
	heading: "Welcome to my portfolio site",
	title: "A web dev from 🇬🇧",
	content: "In blandit nisi iaculis nunc hendrerit tincidunt quis nec enim. Vivamus ullamcorper at mauris at fringilla. Mauris feugiat ultrices mauris, ac lacinia enim bibendum sit amet. Phasellus id arcu eu dolor bibendum mattis sit amet vel justo.",
	videoImage: ../../assets/images/pexels-pavel-danilyuk-5495845.min.png,
	backgroundText: "Who am I?"
}
about: {
	title: "I develop, and much more",
	content: "I help designers, small menium and large agencies and businesses bringing their ideas to life. Equipped with Figma, VS Code and coffee I turn your requirements into websites (EMOJI)",
	backgroundText: "What do I do?"
}
work: {
	image: ../../assets/images/pexels-junior-teixeira-2047905.jpg,
	backgroundText: "Work work work work",
	technologies: [
		{
			title: "Backend",
			content: "In blandit nisi iaculis nunc hendrerit tincidunt quis nec enim.",
			images: [
				{ image: ../../assets/images/home-page-logos/laravel.png},
				{ image: ../../assets/images/home-page-logos/drupal.png},
				{ image: ../../assets/images/home-page-logos/strapi.png},
			],
			graphic: backend,
		},
		{
			title: "Frontend",
			content: "In blandit nisi iaculis nunc hendrerit tincidunt quis nec enim.",
			images: [
				{ image: ../../assets/images/home-page-logos/react.png},
				{ image: ../../assets/images/home-page-logos/gatsby.png},
				{ image: ../../assets/images/home-page-logos/next.png},
			],
			graphic: frontend,
		},
		{
			title: "Server?",
			content: "In blandit nisi iaculis nunc hendrerit tincidunt quis nec enim.",
			images: [
				{ image: ../../assets/images/home-page-logos/aws.png},
				{ image: ../../assets/images/home-page-logos/linux.png},
				{ image: ../../assets/images/home-page-logos/nodejs.png},
			],
			graphic: server,
		},
		{
			title: "Design",
			content: "In blandit nisi iaculis nunc hendrerit tincidunt quis nec enim.",
			images: [
				{ image: ../../assets/images/home-page-logos/figma.png},
			],
			graphic: design,
		},
	]
}
---
