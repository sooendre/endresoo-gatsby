module.exports = [
  {
    resolve: `gatsby-source-filesystem`,
    options: {
      name: `home`,
      path: `${__dirname}/markdown/home`,
    },
  },
  {
    resolve: `gatsby-source-filesystem`,
    options: {
      name: `footer`,
      path: `${__dirname}/markdown/general/footer`,
    },
  },
  {
    resolve: `gatsby-source-filesystem`,
    options: {
      name: `seo`,
      path: `${__dirname}/markdown/general/seo`,
    },
  },
  {
    resolve: `gatsby-source-filesystem`,
    options: {
      path: `${__dirname}/assets/images/`,
    },
  },
];
