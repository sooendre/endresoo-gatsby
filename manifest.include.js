module.exports = {
  theme_color: "#333",
  background_color: "#333",
  display: "standalone",
  scope: "/",
  start_url: "/",
  name: "Endre Soo",
  short_name: "Soo",
  description:
    "Professional full stack web development using the latest web technologies. JS, PHP and popular frameworks, CMS solutions. SQL or NoSQL databases. Figma designs",
  icon: "static/icons/icon-512x512.png",
  icons: [
    {
      src: "static/favicons/favicon-16x16.png",
      sizes: "16x16",
      type: "image/png",
      purpose: "any maskable",
    },
    {
      src: "static/favicons/favicon-32x32.png",
      sizes: "32x32",
      type: "image/png",
      purpose: "any maskable",
    },
    {
      src: "static/icons/icon-192x192.png",
      sizes: "192x192",
      type: "image/png",
      purpose: "any maskable",
    },
    {
      src: "static/icons/icon-256x256.png",
      sizes: "256x256",
      type: "image/png",
      purpose: "any maskable",
    },
    {
      src: "static/icons/icon-384x384.png",
      sizes: "384x384",
      type: "image/png",
      purpose: "any maskable",
    },
    {
      src: "static/icons/icon-512x512.png",
      sizes: "512x512",
      type: "image/png",
      purpose: "any maskable",
    },
  ],
};
