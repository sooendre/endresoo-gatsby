const fileSystem = require("./gatsby-source-files.include");
const manifest = require("./manifest.include");

const responsiveBreakpoints = {
  xs: "(min-width: 576px)",
  sm: "(min-width: 768px)",
  md: "(min-width: 992px)",
  l: "(min-width: 1200px)",
  xl: "(min-width: 1600px)",
  portrait: "(orientation: portrait)",
};

module.exports = {
  siteMetadata: {
    title: `Endre Soo`,
    description: `Professional full stack web development using the latest web technologies. JS, PHP and popular frameworks, CMS solutions. SQL or NoSQL databases. Figma designs`,
    author: `Endre Soo`,
    url: `https://endresoo.com`,
  },
  plugins: [
    `gatsby-transformer-remark`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-emotion`,
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `roboto:100,200,300,400,500, 600,700,800`,
          `montserrat:100,200,300,400,500, 600,700,800`,
        ],
        display: "swap",
      },
    },
    {
      resolve: "gatsby-plugin-breakpoints",
      options: {
        queries: responsiveBreakpoints,
      },
    },
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: `${__dirname}/assets/svgs/`,
        },
      },
    },
    ...fileSystem,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        ...manifest,
      },
    },
    `gatsby-plugin-offline`,
  ],
};
